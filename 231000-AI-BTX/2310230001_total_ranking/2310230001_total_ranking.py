from pathlib import Path
import re


SH          = Path(__file__).parent
published_p = SH.parent

#region helper
def get_kq(txt_p):
  m     = re.findall(r'---region ketqua(.*)---endregion ketqua', txt_p.read_text(), flags=re.DOTALL)
  lines = m[0].strip().split('\n')

  d = {}
  for l in lines:
    v = l.split()

    email     = v[2]
    score     = v[3]
    nohonor   = v[2]
    lop_hoten = ' '.join(v[5:]).title()

    d[email] = {
      'email'     : email,
      'score'     : score,
      'nohonor'   : nohonor,
      'lop_hoten' : lop_hoten,
    }
  return d

def sort_by_score(kq_bailam_d:dict):
  """
  sort dict by value w/ custom lambda  ref https://stackoverflow.com/a/59718555/248616
  """
  return dict(
    sorted(
      kq_bailam_d.items(),
      key=lambda i: float(i[1]['score'] [:-1] if i[1]['nohonor']=='.' else -1)/100.0,
      reverse=True,
    )
  )

def get_kq_sorted(bailam_p):
  return sort_by_score( get_kq(bailam_p) )

#endregion helper


#region build :bailam_d
'''
--- target outcome 
bailam_d = {
  $ten_bailam : {'bailam_p': published_p/'2310220000_ketqua_thi__bai1_get24hformat.txt', 'kq': get_kq_sorted($bailam_p) },
  $ten_bailam : {'bailam_p': published_p/'2310190000_ketqua_thi__bai2_greeting.txt',     'kq': get_kq_sorted($bailam_p) },
  ...
}
'''
bailam_d = {}
for bailam_p in [
  #          /                      __ten_bailam
  published_p/'2310220000_ketqua_thi__bai1_get24hformat.txt',
  published_p/'2310190000_ketqua_thi__bai2_greeting.txt',
]:
  ten_bailam = bailam_p.as_posix().split('__')[1]
  bailam_d[ten_bailam] = {'bailam_p': bailam_p, 'kq': get_kq_sorted(bailam_p) }
#endregion build :bailam_d


#region gather thisinh_list
thisinh_byemail = {}
for ten_bailam,d in bailam_d.items():
  for _,d2 in d['kq'].items():
    s = thisinh_id_full = f'''{d2['email']} {d2['lop_hoten']}'''.strip()

    try:
      s = re.sub('@[^ ]+ ', '@ ', s)
      s = re.sub('Lop',     '',   s)
      s = [ i[:2] for i in s.split('@')[1].strip().split() ]  # eg ['10', 'Hà', 'Ch', 'Dũ']
      s = s[0] + ''.join(i[0] for i in s[1:])
    except:
      s = thisinh_id_full

    thisinh_id = s
    thisinh_byemail[d2['email']] = thisinh_id
#endregion gather thisinh_list


#region cook report
r={}
for email, thisinh_id in thisinh_byemail.items():
  r[email] = {}
  r[email] ['thisinh_id'] = thisinh_id

  total = 0
  for bailam_name, d in bailam_d.items():
    try:
      score = d['kq'][email]['score']
    except:
      score = None
    r[email] [bailam_name] = score

    # compute total
    #      =                            \d %
    total += float(d['kq'][email]['score'] [:-1]) if score else 0

    # lop + hoten
    try:    r[email] ['lop_hoten'] = d['kq'][email]['lop_hoten']
    except: r[email] ['lop_hoten'] = ''

  r[email] ['total'] = total

# sort by :total
r = dict(sorted(r.items(), key=lambda i:i[1]['total'], reverse=True))

thisinh_report_d = r
#endregion cook report


#region print output
print(f''' {'bai1':>4} {'bai2':>4} {'.':>3} {'.':<33}''')
print(f''' {'|':>4} {'|':>4} {'|':>3} {'|':<33}''')

skip_email = ['nam.gi.vu@gmail.com']
i          = 0
for email, d in thisinh_report_d.items():
  if email in skip_email: continue

  score_s = ''
  for ten_bailam,_ in bailam_d.items():
    percent  = d[ten_bailam] if d[ten_bailam] else '.'
    score_s += f''' {percent:>4}'''

  # print(f'''{d['thisinh_id']}{score_s}''')
  print(f'''{score_s} {i+1:>3} {email:<33} {d['lop_hoten']}''')
  i+=1
#endregion print output
